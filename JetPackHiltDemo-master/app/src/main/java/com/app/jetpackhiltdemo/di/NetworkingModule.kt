package com.app.jetpackhiltdemo.di

import android.content.Context
import android.content.SharedPreferences
import android.os.UserManager
import android.preference.PreferenceManager
import androidx.room.Room
import com.app.jetpackhiltdemo.BuildConfig
import com.app.jetpackhiltdemo.application.HiltApplication
import com.app.jetpackhiltdemo.data.local.db.AppDatabase
import com.app.jetpackhiltdemo.data.local.db.DbHelper
import com.app.jetpackhiltdemo.data.local.db.IDbHelper
import com.app.jetpackhiltdemo.data.local.prefs.ISharedPrefsHelper
import com.app.jetpackhiltdemo.data.local.prefs.SharedPrefsHelper
import com.app.jetpackhiltdemo.network.ApiService
import com.app.jetpackhiltdemo.network.NetworkingConstants
import com.app.jetpackhiltdemo.network.NetworkingConstants.BASE_URL
import com.app.jetpackhiltdemo.utility.ConstantData
import com.app.jetpackhiltdemo.utility.PreferencesHelper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object NetworkingModule {


    @Singleton
    @Provides
    fun provideContext(application: HiltApplication): Context = application

    @Singleton
    @Provides
    fun provideDatabaseName() = ConstantData.DB_NAME

    @Singleton
    @Provides
    fun provideDatabaseVersion() = ConstantData.DB_VERSION

    @Singleton
    @Provides
    fun providePrefName() = ConstantData.SHARED_PREF_NAME

    @Singleton
    @Provides
    fun provideAppDatabase(context: Context) =
        Room.databaseBuilder(context, AppDatabase::class.java, ConstantData.DB_NAME)
            .allowMainThreadQueries().build()

    @Singleton
    @Provides
    fun provideDbHelper(appDbHelper: DbHelper): IDbHelper = appDbHelper

    @Singleton
    @Provides
    fun providePreferencesHelper(appPreferencesHelper: SharedPrefsHelper): ISharedPrefsHelper
            = appPreferencesHelper

    @Singleton
    @Provides
    fun provideSharedPrefs(context: Context): SharedPreferences =
        context.getSharedPreferences(ConstantData.SHARED_PREF_NAME, Context.MODE_PRIVATE)




    @Provides
    @Singleton
    fun providesBaseUrl(): String {
        return NetworkingConstants.BASE_URL
    }

    @Provides
    @Singleton
    fun providesLoggingInterceptor(): HttpLoggingInterceptor {
        return if (BuildConfig.DEBUG) HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
        else HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.NONE)
    }


    @Provides
    @Singleton
    fun provideOkHttpClient(loggingInterceptor: HttpLoggingInterceptor,requestInterceptor: Interceptor): OkHttpClient {

        val okHttpClient = OkHttpClient().newBuilder()

        okHttpClient.callTimeout(40, TimeUnit.SECONDS)
        okHttpClient.connectTimeout(40, TimeUnit.SECONDS)
        okHttpClient.readTimeout(40, TimeUnit.SECONDS)
        okHttpClient.writeTimeout(40, TimeUnit.SECONDS)
        okHttpClient.addInterceptor(requestInterceptor)
        okHttpClient.addInterceptor(loggingInterceptor)
        okHttpClient.build()
        return okHttpClient.build()
    }


    @Provides
    @Singleton
    fun provideSharedPreferences(@ApplicationContext context: Context): SharedPreferences =
        PreferenceManager.getDefaultSharedPreferences(context)


    @Provides
    @Singleton
    fun provideRequestInterceptor(
        preferencesHelper: PreferencesHelper
    ): Interceptor =
        Interceptor { chain ->

            var request = chain.request()

            if(request.header("No-Authentication")==null){
                val token = preferencesHelper.getAccessTokenFromPreference();
                if(!token.isNullOrEmpty())
                {
                    val finalToken =  "Bearer "+token
                    request = request.newBuilder()
                        .addHeader("Authorization",finalToken)
                        .build()
                }

            }

             chain.proceed(request)
        }



    @Provides
    @Singleton
    fun provideConverterFactory(): Converter.Factory {
        return GsonConverterFactory.create()
    }

    @Provides
    @Singleton
    fun provideRetrofitClient(okHttpClient: OkHttpClient, baseUrl: String, converterFactory: Converter.Factory): Retrofit {
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(okHttpClient)
            .addConverterFactory(converterFactory)
            .build()
    }

    @Provides
    @Singleton
    fun provideRestApiService(retrofit: Retrofit): ApiService {
        return retrofit.create(ApiService::class.java)
    }
}