package com.app.jetpackhiltdemo.data.local.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.app.jetpackhiltdemo.data.local.db.dao.PersonDAO
import com.app.jetpackhiltdemo.data.model.db.Person
import com.app.jetpackhiltdemo.utility.ConstantData


/**
 * Created by Pravin Divraniya on 10/5/2017.
 */
@Database(entities = [Person::class],
            version = ConstantData.DB_VERSION, exportSchema = true)
abstract class AppDatabase : RoomDatabase() {
    abstract fun personDAO(): PersonDAO
}