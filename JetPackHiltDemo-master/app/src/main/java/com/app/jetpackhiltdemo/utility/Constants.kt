package com.app.jetpackhiltdemo.utility

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.RelativeLayout



/**
 * Created by Pravin Divraniya on 10/5/2017.
 * All are singleton class
 */
object ConstantData {
    const val PACKAGE_NAME = "com.app.jetpackhiltdemo" //BuildConfig.APPLICATION_ID
    const val DB_NAME = "demo-dagger.db"
    const val DB_VERSION = 1
    const val SHARED_PREF_NAME = "app-shared-preference"
    const val NEWS_BASE_URL = "https://newsapi.org/v2/top-headlines"
    const val BASE_URL = "https://jsonplaceholder.typicode.com"
    const val NUM_OF_PAGES = 5
    const val IMAGE_UPLOAD_URL = ""

    /**
     * The desired interval for location updates. Inexact. Updates may be more or less frequent.
     */
    const val UPDATE_INTERVAL: Long = 6000 // Every 6 seconds.

    /**
     * The fastest rate for active location updates. Updates will never be more frequent
     * than this value, but they may be less frequent.
     */
    const val FASTEST_UPDATE_INTERVAL: Long = 3000 // Every 3 seconds

    /**
     * The max time before batched results are delivered by location services. Results may be
     * delivered sooner than this interval.
     */
    const val MAX_WAIT_TIME = UPDATE_INTERVAL * 5 // Every 30 seconds.

}// This utility class is not publicly instantiable

object CommonUtils {

    fun showLoadingDialog(context: Context, rootView: View): ProgressBar {
        val layout = rootView as ViewGroup
        val params = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT)

        val mProgressBar = ProgressBar(context, null, android.R.attr.progressBarStyleLargeInverse)
        mProgressBar.isIndeterminate = true

        val rl = RelativeLayout(context)

        rl.gravity = Gravity.CENTER
        rl.addView(mProgressBar)

        layout.addView(rl, params)

        return mProgressBar
    }
    fun hideLoadingDialog(rootView: ViewGroup?,parentView: View?){
        if(rootView == null || parentView == null)
            return
        rootView.removeView(parentView)
    }

}// This utility class is not publicly instantiable

object NetworkUtils {
    fun isNetworkConnected(context: Context): Boolean {
        var result = false
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            val networkCapabilities = connectivityManager.activeNetwork?:return false
            val activeNetwork = connectivityManager.getNetworkCapabilities(networkCapabilities)?:return false
            result = when{
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                else -> false
            }
        }else{
            @Suppress("DEPRECATION")
            connectivityManager.run {
                connectivityManager.activeNetworkInfo?.run {
                    result = when(type){
                        ConnectivityManager.TYPE_WIFI -> true
                        ConnectivityManager.TYPE_MOBILE -> true
                        ConnectivityManager.TYPE_ETHERNET -> true
                        else -> false
                    }
                }
            }
        }
        return result
    }
}// This class is not publicly instantiable


