package com.app.jetpackhiltdemo.data.manager


import android.content.Context
import com.app.jetpackhiltdemo.data.local.db.DbHelper
import com.app.jetpackhiltdemo.data.local.db.IDbHelper
import com.app.jetpackhiltdemo.data.local.prefs.ISharedPrefsHelper
import com.app.jetpackhiltdemo.data.local.prefs.SharedPrefsHelper
import javax.inject.Inject
import javax.inject.Singleton


/**
 * Created by Pravin Divraniya on 10/5/2017.
 */
@Singleton
class DataManager @Inject constructor(private val context: Context, dbHelper: IDbHelper, sharedPreHelper: ISharedPrefsHelper) :
    IDataManager {
    private val mSharedPrefsHelper: SharedPrefsHelper = sharedPreHelper as SharedPrefsHelper
    private val dbHelper: DbHelper = dbHelper as DbHelper
    
    
    override fun setAccessToken(accessToken: String) {
        mSharedPrefsHelper.setAccessToken(accessToken)
    }
    override fun getAccessToken() = mSharedPrefsHelper.getAccessToken()


    override fun getAllPerson(): List<Person> {
        TODO("Not yet implemented")
    }

    override fun insertPerson(person: Person) {
        TODO("Not yet implemented")
    }

    override fun updatePerson(person: Person) {
        TODO("Not yet implemented")
    }

    override fun deletePerson(person: Person) {
        TODO("Not yet implemented")
    }

    override fun getPerson(id: Long): Person {
        TODO("Not yet implemented")
    }

    override fun getCurrentUserId(): String? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun setCurrentUserId(userId: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getCurrentUserName(): String {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun setCurrentUserName(userName: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getCurrentUserEmail(): String {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun setCurrentUserEmail(email: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getCurrentUserProfilePicUrl(): String {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun setCurrentUserProfilePicUrl(profilePicUrl: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}