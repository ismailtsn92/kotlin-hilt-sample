package com.app.jetpackhiltdemo.network

import com.app.jetpackhiltdemo.model.RepositoriesModel
import retrofit2.http.*

interface ApiService {

    @GET(NetworkingConstants.URL_REPOSITORIES)
    suspend fun getPublicRepositories(
        @Query("since") since: String
    ): RepositoriesModel

}