//package com.app.jetpackhiltdemo.di
//
//import android.content.Context
//import android.content.SharedPreferences
//import androidx.room.Room
//import com.app.jetpackhiltdemo.data.local.db.AppDatabase
//import com.app.jetpackhiltdemo.data.local.db.DbHelper
//import com.app.jetpackhiltdemo.data.local.db.IDbHelper
//import com.app.jetpackhiltdemo.data.local.prefs.ISharedPrefsHelper
//import com.app.jetpackhiltdemo.data.local.prefs.SharedPrefsHelper
//import com.app.jetpackhiltdemo.utility.ConstantData.DB_NAME
//import com.app.jetpackhiltdemo.utility.ConstantData.DB_VERSION
//import com.app.jetpackhiltdemo.utility.ConstantData.SHARED_PREF_NAME
//import dagger.Module
//import dagger.Provides
//import com.app.jetpackhiltdemo.application.HiltApplication
//import javax.inject.Singleton
//
//
//object AppModule {
//    @Singleton
//    @Provides
//    fun provideContext(application: HiltApplication): Context = application
//
//    @Singleton
//    @Provides
//    fun provideDatabaseName() = DB_NAME
//
//    @Singleton
//    @Provides
//    fun provideDatabaseVersion() = DB_VERSION
//
//    @Singleton
//    @Provides
//    fun providePrefName() = SHARED_PREF_NAME
//
//    @Singleton
//    @Provides
//    fun provideAppDatabase(context: Context) =
//        Room.databaseBuilder(context, AppDatabase::class.java, DB_NAME)
//            .allowMainThreadQueries().build()
//
//    @Singleton
//    @Provides
//    fun provideDbHelper(appDbHelper: DbHelper): IDbHelper = appDbHelper
//
//    @Singleton
//    @Provides
//    fun providePreferencesHelper(appPreferencesHelper: SharedPrefsHelper): ISharedPrefsHelper
//            = appPreferencesHelper
//
//    @Singleton
//    @Provides
//    fun provideSharedPrefs(context: Context): SharedPreferences =
//        context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
//
//}