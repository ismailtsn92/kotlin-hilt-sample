package com.app.jetpackhiltdemo.ui.main.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import com.app.jetpackhiltdemo.model.RepositoriesModel
import com.app.jetpackhiltdemo.network.ResultData
import com.app.jetpackhiltdemo.usecase.DataUseCase
import com.app.jetpackhiltdemo.utility.NetworkHelper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flow
import okhttp3.ResponseBody

class MainViewModel @ViewModelInject constructor(
    private val dataUseCase: DataUseCase,
    private val networkHelper: NetworkHelper
): ViewModel() {



    fun getRepositoriesList(since: String): LiveData<ResultData<RepositoriesModel?>> {
        return flow {
            emit(ResultData.Loading())
            try {
                if(networkHelper.isNetworkConnected()){
                    emit(dataUseCase.getRepositoriesList(since = since))
                }else{
                    emit(ResultData.Failed("internet","No internet connection"))
                }

            } catch (e: Exception) {
                e.printStackTrace()
                emit(ResultData.Exception())
            }
        }.asLiveData(Dispatchers.IO)
    }



    //LoginViewModel için ekleyebilirsin.
    fun validateEmail(email: String): String {
        if (email.trim().isEmpty() || email.length == 0) {
            return "Please enter an email"
        }
        return "ok"
    }

    fun validatePassword(password: String): String {
        if (password.trim().isEmpty() || password.length == 0) {
            return "Please enter a password"
        }
        if (password.length < 6) {
            return "Minimum password must be 7 chars"
        }
        return "ok"
    }
}