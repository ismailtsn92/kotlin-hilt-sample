package com.app.jetpackhiltdemo.data.model.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.app.jetpackhiltdemo.data.model.BaseModel
import java.io.Serializable

@Entity(tableName = "mUser")
data class Person(@ColumnInfo(name="person_name") var name:String,
                  @ColumnInfo(name="age") var age:Int): BaseModel(), Serializable {
    @ColumnInfo(name = "id")
    @PrimaryKey(autoGenerate = true)
    var id:Long = 0
}