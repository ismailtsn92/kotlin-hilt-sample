package com.app.jetpackhiltdemo.data.local.db.dao

import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE
import com.app.jetpackhiltdemo.data.model.db.Person


/**
 * Created by Pravin Divraniya on 10/5/2017.
 */
@Dao
interface PersonDAO {

    @Query("select * from mUser")
    fun getAllPerson():List<Person>

    @Insert(onConflict = REPLACE)
    fun insertPerson(person: Person)

    @Update(onConflict = REPLACE)
    fun updatePerson(person: Person)

    @Delete
    fun deletePerson(person: Person)

    @Query("select * from mUser where id = :id")
    fun getPerson(id:Long): Person


}