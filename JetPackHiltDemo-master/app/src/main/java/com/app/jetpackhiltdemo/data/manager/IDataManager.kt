package com.app.jetpackhiltdemo.data.manager

import com.app.jetpackhiltdemo.data.local.db.IDbHelper
import com.app.jetpackhiltdemo.data.local.prefs.ISharedPrefsHelper


/**
 * Created by Pravin Divraniya on 11/13/2017.
 */
interface IDataManager: IDbHelper, ISharedPrefsHelper